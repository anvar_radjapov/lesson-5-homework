//Task 1
function vowel_count (str1)
{
	var vowel_list = "aeiouAEIOU";
	var vcount=0;

	for (var x = 0; x<str1.length; x++)
	{
		if (vowel_list.indexOf(str1[x]) !== -1)
		{vcount += 1}
	}
	return vcount
}
console.log(vowel_count("The quick brown fox"));
//Task 2
today=new Date();
var nyear=new Date(today.getFullYear(), 12, 32);
if (today.getMonth()==12 && today.getDate()>31) 
{
nyear.setFullYear(nyear.getFullYear()+1); 
}  
var one_day=1000*60*60*24;
console.log(Math.ceil((nyear.getTime()-today.getTime())/(one_day))+
" days left until New Year!");
//Task 3
var symbols_arr = ['Apple', '&', 'Volvo', '@', 'Anvar', 'Iphone'];

function checkValue(value, arr) {
    var status = 'Not exist';

    for (var i = 0; i < arr.length; i++) {
        var name = arr[i];
        if (name == value) {
            status = 'Exist';
            break;
        }
    }

    return status;
}

console.log('status : ' + checkValue('&', symbols_arr));
console.log('status : ' + checkValue('@', symbols_arr)); 
// Task 4
function performintersection (arr1, arr2){
	const setA = new Set(arr1);
	const setB = new Set(arr2);
	let intersectionResult = [];
	
	for (let i of setB){
		if (setA.has(i)){
			intersectionResult.push(i);
		}	
	}
	return intersectionResult;
}
const array1 = [1,2,3,4,5,6];
const array2 = [1,3,5,7];

const result = performintersection(array1, array2);
console.log(result);
// Task 5
setTimeout(function(){alert("Hello Udevs")},4000);
//Task 6
var array = [1, 2, 3, 4, 5, 6],
    s = 0,
    p = 1,
    i;
for (i = 0; i < array.length; i += 1) 
   {
    s += array[i];
    }
console.log('Sum : '+s);
//Task 7
function char_count(str, letter) 
{
 var letter_Count = 0;
 for (var position = 0; position < str.length; position++) 
 {
    if (str.charAt(position) == letter) 
      {
      letter_Count += 1;
      }
  }
  return letter_Count;
}

console.log(char_count('anvar', 'a'));